import React, { Component } from 'react';
// import './Menu.css';
//import './Bill.css';

import axios from 'axios';

export default class Order extends Component {


    createOrder = ()=>{
        const data={
            orderitem:this.props.orderItems,
            price:this.props.totalPrice,
            username:'soosoo'
        }
        axios.post('http://jsonplaceholder.typicode.com/posts',data)
        .then(response=>{
            alert('Thank you ')
            this.props.clearOrder()
         console.log(response)  
         })
    }
  render() {

    return (
<div>
    <h2>

  your  order total Price {this.props.totalPrice}
    </h2>
      <ul>
          {this.props.items.map(item=>{
              const img =item.itemImage!='null'? {'background-image':` url(${item.itemImage})`}:{}
              return(
                  <li key={item.itemId} style={img}><div className="title">
                  Price {item.itemPrice} JD
                  
                  </div>
                 
                  </li>
          )})}
       
        
      </ul>
                <button onClick={this.createOrder}>Create order</button>
          </div>


    )
  }
}